package com.system.model;

/**
 * Created by Varamadon on 18.07.2017.
 */
public class Constans {
    public static final double G = (((6.674083131) / 1000000) / 100000) * (Math.pow(60, 2) * 1000);
    public static final Vector3 ZERO = new Vector3(0, 0, 0);
    public static final Vector3 EARTH_RADX = new Vector3(0.006371 + 0.009, 0, 0);
    public static final Vector3 V_INF = new Vector3(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
    public static final double ACTUAL_THRUST_FORCE = 4.5864 * Math.pow(10, -35);
    public static final double ACTUAL_ROCKET_MASS = 33.7 * Math.pow(10, -27);
    public static final double EPSILON_DISTANCE_STRICT = 0.0001;
    public static final double EPSILON_DISTANCE_LIGHT = 0.007;
    public static final double GOLDEN_SECTION_CONSTANT = (1+Math.sqrt(5))/2;
}
