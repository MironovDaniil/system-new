package com.system.model;

import java.util.ArrayList;

/**
 * Created by Varamadon on 30.01.2018.
 */
public class Trajectory {
    private ArrayList<Vector3> trajPoints;
    private boolean isStableOrbit;
    private Vector3 apoaps;
    private Vector3 periaps;
    private double bigHalfAxis;
    private double excentr;
    private Vector3 plainNormal;
    private long period;
    private int timesLooped;
    private int startPointIndex, currentPointIndex;

    public Trajectory() {
        trajPoints = new ArrayList<>(1000);
        isStableOrbit = false;
        apoaps = Constans.ZERO;
        periaps = Constans.V_INF;
        plainNormal = Constans.ZERO;
        period = 0;
        startPointIndex = 0;
        currentPointIndex = -1;
        timesLooped = 0;
        bigHalfAxis = 0;
        excentr = 1;
    }

    public Trajectory(Trajectory other) {
        trajPoints = new ArrayList<>(other.trajPoints.size());
        for (Vector3 vector : other.trajPoints) {
            trajPoints.add(new Vector3(vector));
        }
        isStableOrbit = other.isStableOrbit;
        bigHalfAxis = other.bigHalfAxis;
        period = other.period;
        timesLooped = other.timesLooped;
        startPointIndex = other.startPointIndex;
        currentPointIndex = other.currentPointIndex;
        apoaps = new Vector3(other.apoaps);
        periaps = new Vector3(other.periaps);
        plainNormal = new Vector3(other.plainNormal);
    }

    public void writeTraj(Vector3 point) {
        trajPoints.add(point);
        currentPointIndex++;
        if (trajPoints.get(currentPointIndex).abs() > apoaps.abs()) {
            apoaps = trajPoints.get(currentPointIndex);
        }
        if (trajPoints.get(currentPointIndex).abs() < periaps.abs()) {
            periaps = trajPoints.get(currentPointIndex);
        }
        //System.out.println(trajPoints.get(startPointIndex).add(trajPoints.get(currentPointIndex).multiply(-1)).abs());
        if ((trajPoints.get(startPointIndex).add(trajPoints.get(currentPointIndex).multiply(-1)).abs() < Constans.EPSILON_DISTANCE_LIGHT) && (currentPointIndex - startPointIndex > 20)) {
            //System.out.println("Uspeh!AAAAAA");
            isStableOrbit = true;
            startPointIndex = currentPointIndex;
            period = currentPointIndex - period * timesLooped;
            timesLooped++;
            plainNormal = trajPoints.get(currentPointIndex).vectorMultiply(trajPoints.get(currentPointIndex / 4)).direction();
            bigHalfAxis = (apoaps.abs() + periaps.abs()) / 2;
            excentr = (apoaps.abs() - periaps.abs()) / (apoaps.abs() + periaps.abs());
            //System.out.println(bigHalfAxis);
            //apoAndPeriShow();
            //System.out.println(Math.pow((Math.pow(period,3)*Constans.G*S.getMass())/(4*Math.pow(Math.PI,2)),1.0/3.0));
            //plainNormal.show();
            //System.out.println(period);
            //currentPointIndex = startPointIndex;
            //trajPoints.clear();
        }
        /*else
        {
            //System.out.println("Neuspeh!BBBBBBB");
            isStableOrbit=false;
        }*/
    }

    public boolean checkStability() {
        return isStableOrbit;
    }

    public void setApoaps() {
        if (isStableOrbit) {
            double Max = 0;
            int k = 0;
            for (int i = 0; i < trajPoints.size(); i++) {
                if (trajPoints.get(i).abs() > Max) {
                    Max = trajPoints.get(i).abs();
                    k = i;
                }
            }
            apoaps = trajPoints.get(k);
            System.out.println("Uspeh!");
            apoaps.show();
        } else {
            System.out.println("Trajectory is not stable!");

        }
    }

    public void setPeriaps() {
        if (isStableOrbit) {
            double Min = 2000;
            int k = 0;
            for (int i = 0; i < trajPoints.size(); i++) {
                if (trajPoints.get(i).abs() < Min) {
                    Min = trajPoints.get(i).abs();
                    k = i;
                }
            }
            periaps = trajPoints.get(k);
            System.out.println("Uspeh!");
            periaps.show();
        } else {
            System.out.println("Trajectory is not stable!");
        }
    }

    public void show() {
        for (int i = 0; i < trajPoints.size(); i++) {
            trajPoints.get(i).show();
        }
    }

    public void apoAndPeriShow() {
        apoaps.show();
        periaps.show();
    }

    public double getBigHalfAxis() {
        return bigHalfAxis;
    }

    public void clear() {
        trajPoints.clear();
        apoaps = Constans.ZERO;
        periaps = Constans.V_INF;
        plainNormal = Constans.ZERO;
        period = 0;
        startPointIndex = 0;
        currentPointIndex = -1;
        timesLooped = 0;
        bigHalfAxis = 0;
    }

    public void destabilize() {
        isStableOrbit = false;
    }

    public Vector3 getApoaps() {
        return apoaps;
    }

    public Vector3 getPeriaps() {
        return periaps;
    }

    public double getExcentr() {
        return excentr;
    }

    public Vector3 getPlainNormal() {
        return plainNormal;
    }

    public ArrayList<Vector3> getTrajPoints() {
        return trajPoints;
    }
}
