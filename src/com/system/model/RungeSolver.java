package com.system.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Varamadon on 11.04.2018.
 */
public class RungeSolver {
    public <T> RungeResult solve(List<T> objects, RungeKFunction<T> kFunction, RungeQFunction<T> qFunction,
                                 RungeSumm<T> summ) {
        int N = objects.size();
        List<Vector3> K1 = new ArrayList<>(N);
        List<Vector3> Q1 = new ArrayList<>(N);
        for (T object : objects) {
            K1.add(kFunction.run(object, objects));
        }
        for (T object : objects) {
            Q1.add(qFunction.run(object));
        }
        List<T> OK1 = summ.run(objects, VectorNMethods.vectorNMult(Q1, 0.5));
        List<Vector3> K2 = new ArrayList<>(N);
        List<Vector3> Q2 = new ArrayList<>(N);
        for (T object : OK1) {
            K2.add(kFunction.run(object, OK1));
        }
        for (int i = 0; i < N; i++) {
            Q2.add(qFunction.run(objects.get(i)).add(K1.get(i).multiply(0.5)));
        }
        List<T> OK2 = summ.run(objects, VectorNMethods.vectorNMult(Q2, 0.5));
        List<Vector3> K3 = new ArrayList<>(N);
        List<Vector3> Q3 = new ArrayList<>(N);
        for (T object : OK2) {
            K3.add(kFunction.run(object, OK2));
        }
        for (int i = 0; i < N; i++) {
            Q3.add(qFunction.run(objects.get(i)).add(K2.get(i).multiply(0.5)));
        }
        List<T> OK3 = summ.run(objects, VectorNMethods.vectorNMult(Q3, 1));
        List<Vector3> K4 = new ArrayList<>(N);
        List<Vector3> Q4 = new ArrayList<>(N);
        for (T object : OK3) {
            K4.add(kFunction.run(object, OK3));
        }
        for (int i = 0; i < N; i++) {
            Q4.add(qFunction.run(objects.get(i)).add(K3.get(i).multiply(1)));
        }
        List<Vector3> AdV = VectorNMethods.vectorNMult(VectorNMethods.vectorAdd(K4, VectorNMethods.vectorAdd(VectorNMethods.vectorNMult(K3, 2), VectorNMethods.vectorAdd(K1, VectorNMethods.vectorNMult(K2, 2)))), 0.1666667);
        List<Vector3> AdC = VectorNMethods.vectorNMult(VectorNMethods.vectorAdd(Q4, VectorNMethods.vectorAdd(VectorNMethods.vectorNMult(Q3, 2), VectorNMethods.vectorAdd(Q1, VectorNMethods.vectorNMult(Q2, 2)))), 0.1666667);
        return new RungeResult(AdV, AdC);
    }

    public interface RungeKFunction<T> {
        Vector3 run(T object, List<T> list);
    }

    public interface RungeQFunction<T> {
        Vector3 run(T object);
    }

    public interface RungeSumm<T> {
        List<T> run(List<T> objects, List<Vector3> addedVectors);
    }

    public static class RungeResult {
        private final List<Vector3> vectorK;
        private final List<Vector3> vectorQ;

        public RungeResult(List<Vector3> vectorK, List<Vector3> vectorQ) {
            this.vectorK = vectorK;
            this.vectorQ = vectorQ;
        }

        public List<Vector3> getVectorK() {
            return vectorK;
        }

        public List<Vector3> getVectorQ() {
            return vectorQ;
        }
    }

}
