package com.system.model;

/**
 * Created by Varamadon on 18.07.2017.
 */
public class Planet extends SpaceObject {
    private final double radius;
    private final Double sphereCenterMass;

    private double sphereRadius;

    public Planet(Vector3 coord, Vector3 speed, double mass, double radius, SpaceObject sphereCenter) {
        super(coord, speed, mass);

        this.radius = radius;
        this.sphereCenterMass = sphereCenter != null ? sphereCenter.getMass() : null;
    }

    public Planet(Planet other) {
        super(other);

        this.radius = other.radius;
        this.sphereCenterMass = other.sphereCenterMass;
    }

    private Planet(Vector3 coord, Vector3 speed, double mass, double radius, Double sphereCenterMass) {
        super(coord, speed, mass);
        this.radius = radius;
        this.sphereCenterMass = sphereCenterMass;
    }

    @Override
    public SpaceObject copyNewCoord(Vector3 newCoord) {
        return new Planet(newCoord, getSpeed(), getMass(), getRadius(), sphereCenterMass);
    }

    public double getSphereRadius() {
        if (getTrajectory().checkStability() && sphereCenterMass != null) {
            sphereRadius = Math.exp(Math.log(getMass() / sphereCenterMass) / (5.0 / 2.0)) * getTrajectory().getBigHalfAxis();
            //System.out.println("BBBBBBBBB");
            //System.out.println(sphereRadius);
        }
        return sphereRadius;
    }

    public double getRadius() {
        return radius;
    }

    public boolean ifCrash(SpaceObject spaceObject) {
        return this.getCoord().add(spaceObject.getCoord().multiply(-1)).abs() <= radius;
    }
}
