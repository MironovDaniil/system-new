package com.system.model;

/**
 * Created by Varamadon on 18.07.2017.
 */
public abstract class SpaceObject {

    private final double mass;                        //10^30 килограмм

    private Vector3 coord;                       // 10^6 километров
    private Vector3 speed;
    private Vector3 acceleration;
    private Trajectory trajectory;

    protected SpaceObject(Vector3 coord, Vector3 speed, double mass) {
        this(coord, speed, Constans.ZERO, mass, new Trajectory());
    }

    protected SpaceObject(SpaceObject other) {
        this(
                other.coord,
                other.speed,
                other.acceleration,
                other.mass,
                new Trajectory(other.trajectory));
    }

    private SpaceObject(Vector3 coord,
                        Vector3 speed,
                        Vector3 acceleration,
                        double mass,
                        Trajectory trajectory) {
        this.coord = coord;
        this.speed = speed;
        this.acceleration = acceleration;
        this.mass = mass;
        this.trajectory = trajectory;
    }

    public Vector3 getCoord() {
        return coord;
    }

    public Vector3 getSpeed() {
        return speed;
    }

    public double getMass() {
        return mass;
    }

    public Vector3 getAcceleration() {
        return acceleration;
    }

    public void writeTrajHere() {
        trajectory.writeTraj(this.coord);
    }

    public void setOrbitPoints() {
        trajectory.setApoaps();
        trajectory.setPeriaps();
    }

    public void checkOrbit() {
        trajectory.checkStability();
    }

    public Trajectory getTrajectory() {
        return trajectory;
    }

    protected void setSpeed(Vector3 speed) {
        this.speed = speed;
    }

    protected void manualMove(Vector3 coord, Vector3 speed) {
        this.coord = coord;
        this.speed = speed;
        writeTrajHere();
    }

    protected void setAcceleration(Vector3 acceleration) {
        this.acceleration = acceleration;
    }

    public abstract SpaceObject copyNewCoord(Vector3 newCoord);

    public void move() {
        Vector3 movement = acceleration.multiply(0.5);
        coord = coord.add(speed);
        coord = coord.add(movement);
        speed = speed.add(acceleration);
    }


    public void setYesterday(Vector3 Y) {
    }


    public double pEnergy(SpaceObject Sun) {
        return mass * coord.abs() * OrbitMeth.CountAccel(Sun, this);
    }

    public double kEnergy() {
        return (mass * Math.pow(speed.abs(), 2)) / 2;
    }

    public double energy(SpaceObject Sun) {
        double Pot = mass * (coord.add(Sun.getCoord().multiply(-1))).abs() * OrbitMeth.CountAccel(Sun, this);
        double Kin = (mass * Math.pow(speed.abs(), 2)) / 2;
        return Pot + Kin;
    }

    public void trajPass(SpaceObject other) {
        trajectory = new Trajectory(other.trajectory);
    }
}
