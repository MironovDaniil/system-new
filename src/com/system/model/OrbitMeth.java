package com.system.model;

/**
 * Created by Varamadon on 19.07.2017.
 */
public class OrbitMeth {

        public static double CountAccel(SpaceObject puling, SpaceObject pulled)
        {

            return (((Constans.G*puling.getMass())/(Math.pow(((pulled.getCoord().add(puling.getCoord().multiply(-1))).abs()),2))));
        }

        public static Vector3 VectorAccel(SpaceObject Puling, SpaceObject Pulled)
        {
            double A=OrbitMeth.CountAccel(Puling,Pulled); //Ускорение объекта,милллионов километров в минуту^2
            double X=(A*(((Puling.getCoord().add(Pulled.getCoord().multiply(-1))).getX())/((Pulled.getCoord().add(Puling.getCoord().multiply(-1))).abs())));       //Проэкции ускорения на координатные оси

            double Y=(A*(((Puling.getCoord().add(Pulled.getCoord().multiply(-1))).getY())/((Pulled.getCoord().add(Puling.getCoord().multiply(-1))).abs())));

            double Z=(A*(((Puling.getCoord().add(Pulled.getCoord().multiply(-1))).getZ())/((Pulled.getCoord().add(Puling.getCoord().multiply(-1))).abs())));

            return new Vector3(X,Y,Z);
        }

}
