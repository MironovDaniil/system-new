package com.system.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Varamadon on 29.01.2018.
 */
public class VectorNMethods {
    public static List<Vector3> vectorAdd(List<Vector3> firstAdded, List<Vector3> secondAdded) {
        List<Vector3> result = new ArrayList<>(firstAdded.size());
        for (int i = 0; i < firstAdded.size(); i++) {
            result.add(firstAdded.get(i).add(secondAdded.get(i)));
        }
        return result;
    }

    public static List<Vector3> oneVectorAdd(List<Vector3> vector3List, Vector3 vector3) {
        List<Vector3> result = new ArrayList<>(vector3List.size());
        for (int i = 0; i < vector3List.size(); i++) {
            result.add(vector3List.get(i).add(vector3));
        }
        return result;
    }

    public static List<SpaceObject> objectsPlaceAdd(List<SpaceObject> objects, List<Vector3> vectors) {
        List<SpaceObject> result = new ArrayList<>(objects.size());
        for (int i = 0; i < objects.size(); i++) {
            result.add(objects.get(i).copyNewCoord(objects.get(i).getCoord().add(vectors.get(i))));
        }
        return result;
    }

    public static List<Vector3> getObjCoord(List<SpaceObject> objects) {
        List<Vector3> result = new ArrayList<>(objects.size());
        for (int i = 0; i < objects.size(); i++) {
            result.add(objects.get(i).getCoord());
        }
        return result;
    }

    public static List<Vector3> vectorNMult(List<Vector3> list, double multiplicator) {
        List<Vector3> result = new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++) {
            result.add(list.get(i).multiply(multiplicator));
        }
        return result;
    }
}
