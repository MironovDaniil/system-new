package com.system.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Varamadon on 18.07.2017.
 */
public class ModelSit {
    private List<SpaceObject> objects;
    private Rocket rocket;
    private Planet sun;
    private long time;

    public ModelSit() {
        objects = new ArrayList<>();
        time = 0;
        rocket = null;
    }

    public ModelSit(ModelSit other) {
        time = other.time;
        objects = new ArrayList<>(other.getObjects().size());
        for (SpaceObject O : other.objects) {
            if (O == other.getRocket()) {
                SpaceObject obj = new Rocket((Rocket) O);
                objects.add(obj);
                rocket = (Rocket) obj;
            } else if (O == other.getSun()) {
                SpaceObject obj = new Planet((Planet) O);
                objects.add(obj);
                sun = (Planet) obj;
            } else {
                SpaceObject obj = new Planet((Planet) O);
                objects.add(obj);
            }
        }
    }

    public long whatTime() {
        return time;
    }

    public boolean isSunplace(int i) {
        return objects.size() > i && objects.get(i) == sun;
    }

    public boolean isRocketplace(int i) {
        return objects.size() > i && objects.get(i) == rocket;
    }

    public List<SpaceObject> getObjects() {
        return objects;
    }

    public Rocket getRocket() {
        return rocket;
    }

    public Planet getSun() {
        return sun;
    }

    public void addPlanet(Planet planet) {
        objects.add(planet);

        if (sun == null || planet.getMass() > sun.getMass()) {
            sun = planet;
        }
    }

    public void addRocket(Rocket rocket) {
        if (this.rocket == null) {
            objects.add(rocket);
            this.rocket = rocket;
        } else {
            System.out.println("Already have rocket, doing nothing");
        }
    }

    private SpaceObject findOrbit(SpaceObject pulled) {
        double maxPullForce = 0;
        SpaceObject orbitCenter = null;
        for (SpaceObject object : objects) {
            if (object != pulled) {
                //System.out.println(j+" "+OrbitMeth.CountAccel(objects.get(j), objects.get(i)));
                double currentPullForce = OrbitMeth.CountAccel(object, pulled);
                if (maxPullForce < currentPullForce) {
                    maxPullForce = currentPullForce;
                    //System.out.println(maxPullForce);
                    orbitCenter = object;
                }
            }
        }
        return orbitCenter;
    }

    public void makeRound() {
        for (SpaceObject object : objects) {
            if (object != sun) {
                SpaceObject orbitCenter = findOrbit(object);
                //System.out.println(k);
                double distanceToOrbitCenter = (object.getCoord().add(orbitCenter.getCoord().multiply(-1))).abs();
                double acceleration = OrbitMeth.CountAccel(orbitCenter, object);
                double roundSpeed = Math.sqrt(distanceToOrbitCenter * acceleration);
                //System.out.println(roundSpeed);
                double normZ = Math.random() * 0.9;
                double normX = Math.random() * (1 - normZ);
                double normY = 1 - normX - normZ;
                Vector3 norm = new Vector3(normX,normY,normZ).direction();
                Vector3 dir = object.getCoord().vectorMultiply(norm).direction();
                Vector3 newSpeed = dir.multiply(roundSpeed);
                newSpeed = newSpeed.add(orbitCenter.getSpeed());
                double multiplication = 0.7 + (0.3 * Math.random());
                object.setSpeed(newSpeed.multiply(multiplication));
                if (object==rocket)
                {
                    object.setSpeed(newSpeed.multiply(0.8));
                }
            }
        }
        rocket.setRelatives();
        /*double Er = Earth.getPlace(1).abs();
        double Ae = OrbitMeth.CountAccel(SunMass,Earth);
        double Ve = Math.sqrt(Er*Ae);
        Vector3 NESpeed = new Vector3 (0,Ve,0);
        Earth.setSpeed(NESpeed);
        rocket.setSpeed(NESpeed);
        double Et = Target.getPlace(1).abs();
        double At = OrbitMeth.CountAccel(SunMass,Target);
        double Vt = Math.sqrt(Et*At);
        Vector3 NTSpeed = new Vector3 (0,Vt,0);
        Target.setSpeed(NTSpeed);*/

    }

    public Vector3 calculateAcceleration(SpaceObject accelerated, List<SpaceObject> objects) {
        Vector3 NAccel = Constans.ZERO;
        for (SpaceObject object : objects) {
            if (accelerated != object) {
                NAccel = NAccel.add(OrbitMeth.VectorAccel(object, accelerated));
            }
        }

        if (accelerated == rocket) {
            rocket.setPureAcceleration(NAccel);
            Vector3 thrustAccel = rocket.getThrust().multiply(Math.pow(rocket.getMass(), -1));
            NAccel = NAccel.add(thrustAccel);
            /*if (!thrustAccel.equals(Constans.ZERO))
            {
                System.out.println("Rocket thrust counted:");
                rocket1.getThrust().show();
            }*/

        }

        return NAccel;
    }


    public void nextMinuteRunge() {
        RungeSolver solver = new RungeSolver();
        RungeSolver.RungeResult result = solver.solve(objects,
                new RungeSolver.RungeKFunction<SpaceObject>() {
                    @Override
                    public Vector3 run(SpaceObject object, List<SpaceObject> list) {
                        return calculateAcceleration(object, list);
                    }
                },
                new RungeSolver.RungeQFunction<SpaceObject>() {
                    @Override
                    public Vector3 run(SpaceObject object) {
                        return object.getSpeed();
                    }
                },
                new RungeSolver.RungeSumm<SpaceObject>() {
                    @Override
                    public List<SpaceObject> run(List<SpaceObject> objects, List<Vector3> addedVectors) {
                        return VectorNMethods.objectsPlaceAdd(objects, addedVectors);
                    }
                });
        List<Vector3> addedSpeed = result.getVectorK();
        List<Vector3> addedCoordinate = result.getVectorQ();
        if (sun != null) {
            Vector3 rel = addedCoordinate.get(objects.indexOf(sun)).multiply(-1);
            addedCoordinate = VectorNMethods.oneVectorAdd(addedCoordinate, rel);
        }
        for (int i = 0; i < objects.size(); i++) {
            objects.get(i).manualMove(objects.get(i).getCoord().add(addedCoordinate.get(i)), objects.get(i).getSpeed().add(addedSpeed.get(i)));
        }
    }

    public void nextMinute() {
        //objects.get(1).writeTrajHere();
        nextMinuteRunge();
        //System.out.println(rocket.getPlace(2).abs());
        time++;
        for (SpaceObject object : objects) {
            object.setAcceleration(calculateAcceleration(object, objects));
            //objects.get(i).MoveRunge();
        }
        if ((objects.get(1).getCoord().getY() < 0.001) && (objects.get(1).getCoord().getY() > -0.001)) {
            //System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAA");
            //System.out.println(objects.get(0).getPlace(1).add(objects.get(1).getPlace(1).multiply(-1)).abs());
            //objects.get(1).getPlace(1).show();
            //System.out.println(time);
        }
        if ((time > 195) && (time < 205)) {
            //objects.get(1).checkOrbit();
            //objects.get(1).setOrbitPoints();
            //objects.get(1).getTrajectory().apoAndPeriShow();
        }
        if (rocket.changeOrbitCenterTrigger() || (rocket.getOrbitCenter() == sun)) {
            setRocketOrbitCenter();
        }
    }

    public void nextDayOld() {
        for (int j = 0; j < 1440; j++) {
            for (SpaceObject object : objects) {
                object.setAcceleration(calculateAcceleration(object, objects));
                object.move();
            }
            if ((objects.get(1).getCoord().getY() < 0.1) && (objects.get(1).getCoord().getY() > -0.1)) {
                System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBB");
                objects.get(1).getCoord().show();
            }
            time++;
        }
    }

    public void show() {
        for (int i = 0; i < objects.size(); i++) {
            System.out.println("Object " + i + " coordiantes: ");
            objects.get(i).getCoord().show();
        }
        //System.out.println(objects.get(0).getPlace(1).add(objects.get(1).getPlace(1).multiply(-1)).abs());
        //System.out.println(objects.get(sunplace).getPlace(1).abs());
        //objects.get(sunplace).getPlace(1).show();
        //System.out.println(objects.get(1).energy(objects.get(0)));
    }

    public void passTrajectories(ModelSit other) {
        if (objects.size() != other.objects.size()) {
            System.out.println("Trajectories passing failed!");
        } else {
            for (int i = 0; i < objects.size(); i++) {
                objects.get(i).trajPass(other.objects.get(i));
            }
        }
    }

    public void setRocketOrbitCenter() {
        for (SpaceObject object : objects) {
            if (object != rocket && object != sun) {
                //System.out.println(objects.get(i).getClass());
                if (rocket.setOrbitCenter((com.system.model.Planet) object)) {
                    //System.out.println("Setting  orbit center");
                    break;
                }
            }
        }
        if (rocket.getOrbitCenter() == null) {
            //System.out.println("Setting sun as orbit center");
            rocket.setOrbitCenter(sun);

        }
    }

    public void waitTillTime(long time) {
//        System.out.println("Start time:"+time);
//        System.out.println("Current time:"+this.time);
        if (this.time > time) {
            System.out.println("Waiting failed - needed time already passed");
            return;
        }
        while (this.time < time) {
            this.nextMinute();
        }
    }
}
