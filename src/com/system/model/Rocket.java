package com.system.model;

/**
 * Created by Varamadon on 18.07.2017.
 */
public class Rocket extends SpaceObject {
    private final double maxThrust;
    private Vector3 pureAcceleration;
    private Vector3 thrust;
    private Planet orbitCenter;
    private Vector3 relativeCoord;
    private Vector3 relativeSpeed;
    private Trajectory relativeTrajectory;

    public Rocket(Vector3 coord, Vector3 speed, double mass, double maxThrust) {
        super(coord, speed, mass);

        this.maxThrust = maxThrust;
        this.thrust = Constans.ZERO;
        this.pureAcceleration = Constans.ZERO;
        this.orbitCenter = null;
        this.relativeCoord = coord;
        this.relativeSpeed = speed;
        this.relativeTrajectory = new Trajectory();
    }

    @Override
    public SpaceObject copyNewCoord(Vector3 newCoord) {
        return new Rocket(newCoord, getSpeed(), getMass(), getMaxThrust());
    }

    public Rocket(Rocket other) {
        super(other);

        this.maxThrust = other.getMaxThrust();
        this.thrust = other.getThrust();
        this.orbitCenter = other.orbitCenter == null ? null : new Planet(other.orbitCenter);
        this.relativeCoord = other.getRelativeCoord();
        this.relativeSpeed = other.getRelativeSpeed();
        this.relativeTrajectory = new Trajectory(other.getRelativeTrajectory());
    }

    public boolean setOrbitCenter(Planet probableCenter) {
        if (this.getCoord().add(probableCenter.getCoord().multiply(-1)).abs() > probableCenter.getSphereRadius()) {
            return false;
        } else {
            orbitCenter = probableCenter;
            setRelatives();
            relativeTrajectory.clear();
            return true;
        }
    }

    public boolean changeOrbitCenterTrigger() {
        if (orbitCenter == null) {
            return true;
        } else {
            return (this.getCoord().add(orbitCenter.getCoord().multiply(-1)).abs() > orbitCenter.getSphereRadius());
        }
    }

    public void setRelatives() {
        if (orbitCenter == null) {
            relativeCoord = this.getCoord();
            relativeSpeed = this.getSpeed();
        } else {
            relativeCoord = this.getCoord().add(orbitCenter.getCoord().multiply(-1));
            relativeSpeed = this.getSpeed().add(orbitCenter.getSpeed().multiply(-1));
        }
    }

    public void setPureAcceleration(Vector3 pureAcceleration) {
        this.pureAcceleration = pureAcceleration;
    }

    public Vector3 getThrust() {
        return thrust;
    }

    public double getMaxThrust() {
        return maxThrust;
    }

    public Vector3 getPureAcceleration() {
        return pureAcceleration;
    }

    public void setThrust(Vector3 direction, double power) {
        if ((Math.abs(1 - direction.abs()) > 0.01)&&(!direction.equals(Constans.ZERO))) {
            System.out.println("Rocket#setThrust direction is not normalized");
        }
        //System.out.println("Rocket is thrusting");
        //direction.show();
        /*if (power > maxThrust) {
            //System.out.println("Rocket#setThrust power " + power +
            //        " is larger than max thrust " + maxThrust);
            power = maxThrust;
        }*/
        thrust = direction.multiply(power);
        relativeTrajectory.clear();
        relativeTrajectory.destabilize();
//        getTrajectory().clear();
//        getTrajectory().destabilize();
        //System.out.println("Rocket in thrust:");
        //thrust.show();
    }

    @Override
    protected void manualMove(Vector3 newCoord, Vector3 newSpeed) {
        super.manualMove(newCoord, newSpeed);
        setRelatives();
        if (relativeCoord.abs()<3)
        {
            this.setSpeed(orbitCenter.getSpeed());
            setRelatives();
        }
        relativeTrajectory.writeTraj(relativeCoord);
    }

    public Vector3 getRelativeCoord() {
        return relativeCoord;
    }

    public Vector3 getRelativeSpeed() {
        return relativeSpeed;
    }

    public Trajectory getRelativeTrajectory() {
        return relativeTrajectory;
    }

    public Planet getOrbitCenter() {
        return orbitCenter;
    }

    @Override
    public void trajPass(SpaceObject other) {
        super.trajPass(other);
        Rocket r = (Rocket) other;
        relativeTrajectory = new Trajectory(r.relativeTrajectory);
    }
}
