package com.system.model;

/**
 * Created by Varamadon on 18.07.2017.
 */
public class Vector3 {
    private final double x;
    private final double y;
    private final double z;

    public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3(Vector3 other) {
        this(other.x, other.y, other.z);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public Vector3 add(Vector3 vector) {
        return new Vector3((x + vector.x), (y + vector.y), (z + vector.z));
    }

    public double scalarMultiply(Vector3 vector) {
        return x * vector.x + y * vector.y + z * vector.z;
    }

    public Vector3 vectorMultiply(Vector3 vector) {
        return new Vector3(
                z * vector.y - y * vector.z,
                x * vector.z - z * vector.x,
                y * vector.x - x * vector.y);
    }

    public double abs() {
        return Math.sqrt(this.scalarMultiply(this));
    }

    public Vector3 multiply(double a) {
        return new Vector3(x * a, y * a, z * a);
    }

    public Vector3 direction() {
        if (this.abs() == 0) {
            return Constans.ZERO;
        } else {
            return this.multiply(1 / this.abs());
        }
    }

    public double dist(Vector3 vector) {
        return this.add(vector.multiply(-1)).abs();
    }

    public Vector3 ortVectorXYPlain() {
        return this.vectorMultiply(new Vector3(0, 0, -1)).direction();
    }

    public void show() {
        System.out.println("x:"+x);
        System.out.println("y:"+y);
        System.out.println("z:"+z);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector3 vector3 = (Vector3) o;

        if (Double.compare(vector3.x, x) != 0) return false;
        if (Double.compare(vector3.y, y) != 0) return false;
        return Double.compare(vector3.z, z) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}

