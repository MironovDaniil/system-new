package com.system.graphics;

import com.system.flightControl.ManeuverNode;
import com.system.flightControl.ManeuverPlanner;
import com.system.flightControl.NodeSeries;
import com.system.flightControl.SingularNode;
import com.system.model.*;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GMain extends Application {
    private static final double W = 1300;
    private static final double H = 700;
    private static final double DS = 40;
    private static final double DP = 6;

    private final Canvas canvas = new Canvas(W, H);

    @Override
    public void start(Stage stage) {
        stage.setScene(new Scene(new Group(canvas)));
        stage.show();

        ShowSit();
    }

    private void ShowSit() {
        final Thread thread = new Thread(() -> {
            ModelSit that = IniSit();
            that.makeRound();
            Vector3 roundThrust = new
                    Vector3(-5.876270908674991E-8, 0.00257549919651115, -0.0);
            Vector3 planetReach = new Vector3(-1.1930881313844177E-7,0.0033087609457473363,0);
            long roundTime = 170167;
            long apoTime = (long) (roundTime * 1.75);
            SingularNode circulizer = new SingularNode(roundThrust,roundTime,that);
            //That.show();
            //That.chekIndexTest();

            ManeuverPlanner burner = new ManeuverPlanner(that);
            //SingularNode burning = burner.singularNodeTestCalc(apoTime);
            SingularNode burning = null;
            //burner.predictTrajectory();
            //System.out.println("Periaps height:"+that.getRocket().getTrajectory().getPeriaps().abs());
            Vector3 newPlainNormal = (new Vector3(1, 0, 0)).direction();
            //SingularNode apochange = burner.simpleApoChange(200, ManeuverPlanner.ManeuverType.REAL);
            //NodeSeries test = burner.seriesTest();
            Vector3 direction = new Vector3(1, 0, 0);
            SingularNode reacher = null;
            //burner.waitTillPeriaps(That);

            long k = 0;
            that.getRocket().getTrajectory().clear();
            boolean calculated=false;
            while (true) {
                try {
                    Thread.sleep(7);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (k % 4 == 0) {
                    draw(that);
                    //System.out.println("Rocket relative apoaps abs:"+ That.getRocket().getRelativeTrajectory().getApoaps().abs());
                    //That.getRocket().getRelativeCoord().show();
                    //System.out.println("Rocket relative height:"+that.getRocket().getRelativeCoord().abs());
                }

                for (int i = 0; i < 1440; i++) {
                    if (!circulizer.checkEnded())
                    {
                        circulizer.execute(that.whatTime());
                    }
                    else if (!calculated){
                        long planetTime = (long) (roundTime * 1.2);
                        ManeuverPlanner opit = new ManeuverPlanner(that);
                        reacher = opit.changeTime(planetReach,planetTime);
                        calculated = true;
                        System.out.println("CALCULATED");
                    }
                    else {
                        reacher.execute(that.whatTime());
                    }
                    if ((reacher!=null)&&(reacher.checkEnded()))
                    {
                        double dist = -10;
                        for (SpaceObject object:that.getObjects()) {
                            if ((object!=that.getRocket())&&(object!=that.getSun()))
                            {
                                dist = object.getCoord().add(that.getRocket().getCoord().multiply(-1)).abs();
                            }
                        }
                        //System.out.println("Dist to planet:"+dist);
                    }
                    //reacher.execute(that.whatTime());
                    //burning.execute(that.whatTime());
                    //apochange.execute(that.whatTime());
                    //test.execute(that.whatTime());
                    //burner.compensate();
                    //burner.manualControl(direction,That.getRocket().getMaxThrust());
                    that.nextMinute();
                    //System.out.println(That.getObjects().get(4).getTrajectory().checkStability());
                }
                /*System.out.println("Real rocket coordinates:");
                That.getRocket().getPlace(2).show();
                System.out.println("Relative rocket coordinates:");
                That.getRocket().getRelativeSpeed().show();*/
                //That.show();


            }
        });

        thread.start();
    }

    private ModelSit IniSit() {
        double marsX = 200;               //249.232;
        double marsY = 0;
        double marsZ = 0;
        double rocketX = 152.2; //71.62345203847312;
        double rocketY = 0;
        double rocketZ = 0;
        double marsSpeedX = 0;
        double marsSpeedY = 0;//   0.078534;
        double marsSpeedZ = 0;
        double earthMass = 0.000005972;
        double sunMass = earthMass * 332940;                      //1.989;
        double marsMass = 0.107 * earthMass;

        Vector3 marsPosition = new Vector3(marsX, marsY, marsZ);
        Vector3 rocketPosition = new Vector3(rocketX, rocketY, rocketZ);

        Vector3 marsSpeed = new Vector3(marsSpeedX, marsSpeedY, marsSpeedZ);

        Planet sun = new Planet(Constans.ZERO, Constans.ZERO, sunMass, 0.6597, null);

        Planet mars = new Planet(marsPosition, marsSpeed, marsMass, 0.00339, sun);
        Planet newPlanet = new Planet(new Vector3(400, 0, 0), Constans.ZERO, marsMass, 0.00339, sun);
        Rocket rocket = new Rocket(rocketPosition, Constans.ZERO, Constans.ACTUAL_ROCKET_MASS, Constans.ACTUAL_THRUST_FORCE * 80D);
        Planet newPlanet1 = new Planet(new Vector3(90, 0, 0), Constans.ZERO, marsMass, 0.00339, sun);
        Planet newPlanet2 = new Planet(new Vector3(0, 100, 0), Constans.ZERO, marsMass, 0.00339, sun);

        ModelSit real = new ModelSit();
        real.addPlanet(sun);
        real.addPlanet(createEarth(sun));
        real.addPlanet(mars);
        real.addRocket(rocket);
        real.addPlanet(newPlanet);
        real.addPlanet(newPlanet1);
        real.addPlanet(newPlanet2);
        return real;
    }

    private static Planet createEarth(Planet centerMass) {
        double earthX = 152.098232 + 10;
        double earthY = 0;
        double earthZ = 0;

        double earthSpeedX = 0;
        double earthSpeedY = 0;   //0.107218;
        double earthSpeedZ = 0;

        double earthMass = 0;           //0.000005972;
        Vector3 earthPosition = new Vector3(earthX, earthY, earthZ);
        Vector3 earthSpeed = new Vector3(earthSpeedX, earthSpeedY, earthSpeedZ);

        return new Planet(earthPosition, earthSpeed, earthMass, 0.006371, centerMass);
    }

    private void draw(ModelSit real) {
        final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();

        double x = 0;
        double y = 0;
        graphicsContext.setFill(Color.BLACK);
        graphicsContext.fillRect(0, 0, W, H);
        /*graphicsContext.setFill(Color.GOLD);
        graphicsContext.fillOval((W/2)-(DS/2), (H/2)-(DS/2), DS, DS);
        graphicsContext.setFill(Color.BROWN);*/
        for (int i = 0; i < real.getObjects().size(); i++) {
            if (!real.isRocketplace(i) && !real.isSunplace(i)) {
                graphicsContext.setFill(Color.DARKBLUE);
                x = (W / 2) + (real.getObjects().get(i).getCoord().getX()) - (DP / 2);
                y = (H / 2) + (real.getObjects().get(i).getCoord().getY()) - (DP / 2);
                graphicsContext.fillOval(x, y, DP, DP);
            } else if (real.isSunplace(i)) {
                graphicsContext.setFill(Color.GOLD);
                x = (W / 2) + (real.getObjects().get(i).getCoord().getX()) - (DS / 2);
                y = (H / 2) + (real.getObjects().get(i).getCoord().getY()) - (DS / 2);
                graphicsContext.fillOval(x, y, DS, DS);
            } else if (real.isRocketplace(i)) {
                graphicsContext.setFill(Color.RED);
                x = (W / 2) + (real.getObjects().get(i).getCoord().getX()) - (2 / 2);
                y = (H / 2) + (real.getObjects().get(i).getCoord().getY()) - (2 / 2);
                graphicsContext.fillOval(x, y, 2, 2);
            }
        }
//        for (SpaceObject object:real.getObjects()) {
//            if ((object==real.getRocket()))
//            {
//                for (int i = 0; i < object.getTrajectory().getTrajPoints().size(); i++) {
//                    if (i % 7440 == 0) {
//                        Vector3 point = object.getTrajectory().getTrajPoints().get(i);
//                        graphicsContext.setFill(Color.RED);
//                        x = (W / 2) + (point.getX());
//                        y = (H / 2) + (point.getY());
//                        graphicsContext.fillOval(x, y, 1, 1);
//                    }
//                }
//            }
//            if ((!(object==real.getSun()))&&(!(object==real.getRocket())))
//            {
//                for (int i = 0; i < object.getTrajectory().getTrajPoints().size(); i++) {
//                    if (i % 11440 == 0) {
//                        Vector3 point = object.getTrajectory().getTrajPoints().get(i);
//                        graphicsContext.setFill(Color.RED);
//                        x = (W / 2) + (point.getX());
//                        y = (H / 2) + (point.getY());
//                        graphicsContext.fillOval(x, y, 1, 1);
//                    }
//                }
//            }
//        }

        /*double x1=(W/2)+(real.PlState(1).getPlace(1).get(1))-(DP/2);
        double y1=(H/2)+(real.PlState(1).getPlace(1).get(2))-(DP/2);
        graphicsContext.fillOval(x1, y1, DP, DP);
        graphicsContext.setFill(Color.BROWN);
        double x2=(W/2)+(real.PlState(2).getPlace(1).get(1))-(DP/2);
        double y2=(H/2)+(real.PlState(2).getPlace(1).get(2))-(DP/2);
        graphicsContext.fillOval(x2, y2, DP, DP);
        graphicsContext.setFill(Color.RED);
        double xr=(W/2)+(real.GetRocket().getPlace(1).get(1))-(1);
        double yr=(H/2)+(real.GetRocket().getPlace(1).get(2))-(1);
        graphicsContext.fillOval(xr, yr, 2, 2);*/

    }

    public static void main(String[] args) {
        launch(args);
    }
}
