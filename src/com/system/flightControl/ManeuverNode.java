package com.system.flightControl;

import com.system.model.Constans;
import com.system.model.Rocket;
import com.system.model.Vector3;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Varamadon on 24.02.2018.
 */
public class ManeuverNode {
    private Queue<Vector3> thrusts;
    private Rocket rocket;
    private long startTime;
    private double deltaV;
    private Vector3 dir;
    private Vector3 plannedSpeed;
    private VesselState wished;
    private boolean isExecuting;
    private boolean fail;

    public ManeuverNode(VesselState wish, Rocket rocket) {
        this(new LinkedList<>(), rocket, wish.getTime(), 0, Constans.ZERO, wish.getSpeed(), wish, false, false);
        calculate();
    }

    public ManeuverNode(Queue<Vector3> thrusts,
                        Rocket rocket,
                        long startTime,
                        double deltaV,
                        Vector3 dir,
                        Vector3 plannedSpeed,
                        VesselState wished,
                        boolean isExecuting,
                        boolean fail) {
        this.thrusts = thrusts;
        this.rocket = rocket;
        this.startTime = startTime;
        this.deltaV = deltaV;
        this.dir = dir;
        this.plannedSpeed = plannedSpeed;
        this.wished = wished;
        this.isExecuting = isExecuting;
        this.fail = fail;
    }

    public void calculate() {
        //System.out.println("Node calculation start");
        thrusts.clear();
        //System.out.println("Speed:");
        //rocket.getSpeed().show();
        //System.out.println("Relative speed:");
        //rocket.getRelativeSpeed().show();
        deltaV = wished.getSpeed().add(plannedSpeed.multiply(-1)).abs();
        //System.out.println("In node delta v:"+deltaV/rocket.getMass());
        dir = wished.getSpeed().add(plannedSpeed.multiply(-1)).direction();
        double cap = rocket.getMaxThrust() / rocket.getMass();
        int stepsCount;
        if (deltaV < cap) {
            stepsCount = 1;
            thrusts.offer(dir.multiply(deltaV * rocket.getMass()));
            startTime = wished.getTime() - 1;
            //System.out.println("Low deltaV, created 1 step");
        } else {
            double d = deltaV / cap;
            int a = (int) d;
            stepsCount = a + 2;
            if (stepsCount > 100) {
                fail = true;
                System.out.println("FAIL NODE");
            }
            double sidethrust = (deltaV - (cap * a)) / 2;
            thrusts.offer(dir.multiply(sidethrust * rocket.getMass()));
            for (int i = 0; i < a; i++) {
                thrusts.offer(dir.multiply(cap * rocket.getMass()));
            }
            thrusts.offer(dir.multiply(sidethrust * rocket.getMass()));
            startTime = wished.getTime() - (stepsCount / 2);
            //System.out.println("High delta v, created "+stepsCount+" steps");
        }
    }

    private boolean trainGone(long currentTime) {
        if ((currentTime > startTime) && (!isExecuting)) {
            fail = true;
            return true;
        } else return false;
    }

    public boolean checkFail() {
        return fail;
    }

    public boolean execute(long currentTime)     //все закончилось - false, ещё нет - true
    {

        if (trainGone(currentTime)) {
            System.out.println("FAIL node");
            return false;
        }
        if (fail) {
            System.out.println("FAIL node");
            return false;
        }
        if (thrusts.peek() == null) {
            isExecuting = false;
            System.out.println("Inside node ended");
            return false;
        }
        if (currentTime >= startTime) {
            System.out.println("In node delta v:" + deltaV / rocket.getMass());
            //System.out.println("In node thrust");
            //System.out.println("Steps count:"+thrusts.size());
            //System.out.println("Start time:"+startTime);
            //System.out.println("Wished time:"+wished.getTime());
            Vector3 thrust = thrusts.poll();
            //System.out.println("In node thrust:");
            //thrust.show();
            rocket.setThrust(thrust.direction(), thrust.abs());
            isExecuting = true;
            return true;
        } else {
            //System.out.println("Node waiting");
            return true;
        }
    }
}
