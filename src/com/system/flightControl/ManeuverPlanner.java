package com.system.flightControl;

import com.system.model.*;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Varamadon on 24.02.2018.
 */
public class ManeuverPlanner {
    private ModelSit realSit;
    private ModelSit virtSit;
    private ModelSit deepVirtualSit;
    private Rocket realVessel;
    private Rocket virtVessel;
    private Rocket deepVirtualVessel;
    private Planet target;

    public enum ManeuverType {VIRTUAL, REAL}

    ;

    public ManeuverPlanner(ModelSit real) {
        //this(real,real.getRocket(),new ModelSit(real),virtSit.getRocket(),null);
        realSit = real;
        realVessel = real.getRocket();
        virtSit = new ModelSit(real);
        virtVessel = virtSit.getRocket();
        deepVirtualSit = new ModelSit(real);
        deepVirtualVessel = deepVirtualSit.getRocket();
        target = null;
    }

    public ManeuverPlanner(ModelSit realSit, ModelSit virtSit, Rocket realVessel, Rocket virtVessel, Planet target) {
        this.realSit = realSit;
        this.virtSit = virtSit;
        this.realVessel = realVessel;
        this.virtVessel = virtVessel;
        this.target = target;
    }

//    public NodeSeries burnMax() {
//        //virtSit.show();
//        //virtVessel.getPlace(1).show();
//        waitTillPeriapsNoTrajectory(virtSit);
//        //virtSit.show();
//        VesselState wish = new VesselState
//                (virtVessel.getRelativeSpeed().multiply(1.0000005), virtVessel.getRelativeCoord(), virtSit.whatTime());
//
//        Queue<VesselState> nodes = new LinkedList<>();
//        nodes.offer(wish);
//        //virtVessel.getPlace(1).show();
//        //realVessel.getPlace(2).show();
//        //wish.show();
//        return new NodeSeries(nodes, realSit);
//    }

    public void manualControl(Vector3 direction, double abs) {
        realVessel.setThrust(direction, abs);
    }

    public void waitTillApoaps(ModelSit that) {
        while (that.getRocket().getRelativeCoord().dist(that.getRocket().getRelativeTrajectory().getApoaps())
                > Constans.EPSILON_DISTANCE_STRICT) {
            //System.out.println("Waiting till apoaps:");
            //that.getRocket().getRelativeTrajectory().getApoaps().show();
            that.nextMinute();
        }
    }

    public void waitTillApoapsNoTrajectory(ModelSit that) {
        ModelSit last = new ModelSit(that);
        that.nextMinute();
        if (that.getRocket().getRelativeCoord().abs() > last.getRocket().getRelativeCoord().abs()) {
            while (that.getRocket().getRelativeCoord().abs() > last.getRocket().getRelativeCoord().abs()) {
                //System.out.println("Waiting till apoaps no traj, max now:" + that.getRocket().getRelativeCoord().abs());
                that.nextMinute();
                last.nextMinute();
                if (that.getRocket().getRelativeCoord().abs() > 500) {
                    System.out.println("Apoaps waiting cap reached!");
                    break;
                }
            }
            that = last;
        } else {
            while (that.getRocket().getRelativeCoord().abs() < last.getRocket().getRelativeCoord().abs()) {
                //System.out.println("Something's wrong1");
                that.nextMinute();
                last.nextMinute();
            }
            while (that.getRocket().getRelativeCoord().abs() > last.getRocket().getRelativeCoord().abs()) {
                ///System.out.println("Something's wrong2, abs now:"+that.getRocket().getRelativeCoord().abs());
                that.nextMinute();
                last.nextMinute();
            }
            that = last;
        }
        //System.out.println("Current apoaps:" + that.getRocket().getRelativeCoord().abs());
    }

    public void waitTillPeriapsNoTrajectory(ModelSit that) {
        ModelSit last = new ModelSit(that);
        that.nextMinute();
        if (that.getRocket().getRelativeCoord().abs() < last.getRocket().getRelativeCoord().abs()) {
            while (that.getRocket().getRelativeCoord().abs() < last.getRocket().getRelativeCoord().abs()) {
                that.nextMinute();
                last.nextMinute();
            }
            that = last;
        } else {
            while (that.getRocket().getRelativeCoord().abs() > last.getRocket().getRelativeCoord().abs()) {
                that.nextMinute();
                last.nextMinute();
            }
            while (that.getRocket().getRelativeCoord().abs() < last.getRocket().getRelativeCoord().abs()) {
                that.nextMinute();
                last.nextMinute();
            }
            that = last;
        }
    }

    public void waitTillPeriaps(ModelSit that) {
        while (that.getRocket().getRelativeCoord().dist(that.getRocket().getRelativeTrajectory().getPeriaps()) > Constans.EPSILON_DISTANCE_STRICT) {
            //System.out.println("Waiting till periaps:");
            //that.getRocket().getRelativeTrajectory().getPeriaps().show();
            that.nextMinute();
        }
    }

    public void moveForward(ModelSit that) {
        SpaceObject spaceObject = that.getObjects().get(1);
        double d = 0;
        for (SpaceObject o : that.getObjects()) {
            if (o.getCoord().abs() > d) {
                d = o.getCoord().abs();
                spaceObject = o;
            }
        }
        while (!spaceObject.getTrajectory().checkStability()) {
            //System.out.println("Predicting trajectories");
            that.nextMinute();
        }
    }

    public void moveForwardRocket(ModelSit that) {
        while (!that.getRocket().getRelativeTrajectory().checkStability()) {
            //System.out.println("Predicting trajectories");
            that.nextMinute();
        }
    }

    public void predictTrajectory() {
        moveForward(virtSit);
        realSit.passTrajectories(virtSit);
        virtualModelReset();
    }

    public void testNode(ManeuverNode node) {
        boolean b = true;
        while (b) {
            //System.out.println("Node testing");
            b = node.execute(virtSit.whatTime());
            virtSit.nextMinute();
        }
    }

    private void virtualModelReset() {
        virtSit = new ModelSit(realSit);
        virtVessel = virtSit.getRocket();
    }

    private void deepVirtualModelReset() {
        deepVirtualSit = new ModelSit(realSit);
        deepVirtualVessel = deepVirtualSit.getRocket();
    }

    public void compensate() {
        Vector3 acceleration = realVessel.getPureAcceleration().multiply(realVessel.getMass());
        //System.out.println("Rocket pure acceleration:");
        //acceleration.show();
        Vector3 direction = acceleration.multiply(-1).direction();
        double abs = acceleration.abs();
        //System.out.println("Rocket input thrust:");
        //direction.multiply(abs).show();
        //System.out.println("Difference:"+acceleration.add(direction.multiply(abs)).abs());
        manualControl(direction, abs);
    }

    public SingularNode singularNodeTestCalc(long startTime, double speedMult) {
//        waitTillPeriapsNoTrajectory(virtSit);
//        long startTime = virtSit.whatTime();
//        System.out.println("Peri time:"+startTime);
//        virtualModelReset();
        virtSit.waitTillTime(startTime);
        Vector3 wishedSpeed = ortXYSpeed(virtVessel);
        //System.out.println("Perpend wished speed:"+wishedSpeed.abs());
        //wishedSpeed.show();
        //System.out.println("Old speed:"+virtVessel.getRelativeSpeed().abs());
        //virtVessel.getRelativeSpeed().show();
        //System.out.println("Difference:"+(wishedSpeed.abs()-virtVessel.getRelativeSpeed().abs()));
        //System.out.println(wishedSpeed.dist(virtVessel.getRelativeSpeed()));
        VesselState wish = new VesselState(virtVessel.getRelativeSpeed().multiply(speedMult)
                , virtVessel.getRelativeCoord(), virtSit.whatTime());
        virtualModelReset();
        return new SingularNode(wish, realSit);
    }

    private Vector3 ortXYSpeed(Rocket rocket) {
        Vector3 result = rocket.getRelativeCoord().ortVectorXYPlain();
        if (rocket.getRelativeSpeed().scalarMultiply(result) < 0) {
            result = result.multiply(-1);
        }
        return result.multiply(rocket.getRelativeSpeed().abs());
    }

    private double apoFindCheckFunction(Vector3 direction, double abs, double heightNeeded, long startTime) {
        //System.out.println("==========================");
        //System.out.println("Check started!");
        virtSit.waitTillTime(startTime - 1);
        //System.out.println("Model moved to start time");
        virtVessel.setThrust(direction, abs * virtVessel.getMass());
        //System.out.println("Rocket thrusting:" + abs);
        //direction.show();
        virtSit.nextMinute();
        virtVessel.setThrust(Constans.ZERO, 0);
        //System.out.println("Rocket thrusted");
        waitTillApoapsNoTrajectory(virtSit);
        double result = Math.abs(heightNeeded - virtVessel.getRelativeCoord().abs());
        if (result < 1.E-6) {
            result = 0;
        }
        //System.out.println("Check complete, dist to wish:" + result);
        /*if(result>240)
        {
            result = result - (virtSit.whatTime()* 1.E-3);
            System.out.println("Cap reached, adding corrected time:"+virtSit.whatTime()* 1.E-3);
        }*/
        //System.out.println("Check complete, dist to wish:" + result);
        virtualModelReset();
        //System.out.println("==========================");
        return result;
    }

    private double periFindCheckFunction(Vector3 direction, double abs, double heightNeeded, long startTime) {
        //System.out.println("==========================");
        //System.out.println("Check started!");
        virtSit.waitTillTime(startTime - 1);
        //System.out.println("Model moved to start time");
        virtVessel.setThrust(direction, abs * virtVessel.getMass());
        //System.out.println("Rocket thrusting:" + abs);
        //direction.show();
        virtSit.nextMinute();
        virtVessel.setThrust(Constans.ZERO, 0);
        //System.out.println("Rocket thrusted");
        waitTillPeriapsNoTrajectory(virtSit);
        double result = Math.abs(heightNeeded - virtVessel.getRelativeCoord().abs());
        if (result < 1.E-6) {
            result = 0;
        }
        //System.out.println("Check complete, dist to wish:" + result);
        /*if(result>240)
        {
            result = result - (virtSit.whatTime()* 1.E-3);
            System.out.println("Cap reached, adding corrected time:"+virtSit.whatTime()* 1.E-3);
        }*/
        //System.out.println("Check complete, dist to wish:" + result);
        virtualModelReset();
        //System.out.println("==========================");
        return result;
    }

    private double circulizeCheckFunction(Vector3 direction, double abs, long startTime) {
        virtSit.waitTillTime(startTime - 1);
        //System.out.println("Model moved to start time");
        virtVessel.setThrust(direction, abs * virtVessel.getMass());
        //System.out.println("Rocket thrusting:" + abs);
        //direction.show();
        virtSit.nextMinute();
        virtVessel.setThrust(Constans.ZERO, 0);
        moveForwardRocket(virtSit);
        double result = Math.abs(virtVessel.getRelativeTrajectory().getApoaps().abs()
                - virtVessel.getRelativeTrajectory().getPeriaps().abs());
        if (result < 1.E-6) {
            result = 0;
        }
        virtualModelReset();
        return result;

    }

    public SingularNode apoChange(double newHeight, long startTime, ManeuverType type) {
        if (newHeight < virtVessel.getRelativeTrajectory().getPeriaps().abs()) {
            System.out.println("Too low for apoaps, use periapschanger");
            return null;
        }
        LongSimpleManeuver apoFinder = new LongSimpleManeuver(new GoldenSection());
        virtSit.waitTillTime(startTime);
        Vector3 direction;
        if (newHeight > virtVessel.getRelativeTrajectory().getApoaps().abs()) {
            direction = virtVessel.getRelativeSpeed().direction();
        } else {
            direction = virtVessel.getRelativeSpeed().direction().multiply(-1);
        }


        virtualModelReset();
        ModelSit returnModel;
        switch (type) {
            case REAL:
                returnModel = realSit;
                break;
            case VIRTUAL:
                returnModel = deepVirtualSit;
            default:
                returnModel = null;
        }
        return apoFinder.calculate(returnModel, direction, startTime,
                new LongSimpleManeuver.CheckFunction() {
                    @Override
                    public double run(double thrustAbs) {
                        return apoFindCheckFunction(direction, thrustAbs, newHeight, startTime);
                    }
                });

    }

    public SingularNode periChange(double newHeight, long startTime, ManeuverType type) {
        if (newHeight > virtVessel.getRelativeTrajectory().getApoaps().abs()) {
            System.out.println("Too high for periaps, use apoapschanger");
            return null;
        }
        LongSimpleManeuver periFinder = new LongSimpleManeuver(new GoldenSection());
        virtSit.waitTillTime(startTime);
        Vector3 direction;
        if (newHeight > virtVessel.getRelativeTrajectory().getPeriaps().abs()) {
            direction = virtVessel.getRelativeSpeed().direction();
        } else {
            direction = virtVessel.getRelativeSpeed().direction().multiply(-1);
        }

        virtualModelReset();
        ModelSit returnModel;
        switch (type) {
            case REAL:
                returnModel = realSit;
                break;
            case VIRTUAL:
                returnModel = deepVirtualSit;
            default:
                returnModel = null;
        }
        return periFinder.calculate(returnModel, direction, startTime,
                new LongSimpleManeuver.CheckFunction() {
                    @Override
                    public double run(double thrustAbs) {
                        return periFindCheckFunction(direction, thrustAbs, newHeight, startTime);
                    }
                });

    }

    public SingularNode simpleApoChange(double newHeight, ManeuverType type) {
        waitTillPeriapsNoTrajectory(virtSit);
        SingularNode result = apoChange(newHeight, virtSit.whatTime(), type);
        virtualModelReset();
        return result;
    }

    public SingularNode simplePeriChange(double newHeight, ManeuverType type) {
        waitTillApoapsNoTrajectory(virtSit);
        SingularNode result = periChange(newHeight, virtSit.whatTime(), type);
        virtualModelReset();
        return result;
    }

    public SingularNode circulizeAtPeriaps(ManeuverType type) {
        LongSimpleManeuver circulizer = new LongSimpleManeuver(new GoldenSection());
        waitTillPeriapsNoTrajectory(virtSit);
        Vector3 direction;
        direction = virtVessel.getRelativeSpeed().direction().multiply(-1);
        long startTime = virtSit.whatTime();
        virtualModelReset();
//        System.out.println("Stat time:"+startTime);
//        System.out.println("Current time"+realSit.whatTime());
        switch (type) {
            case REAL:
                return circulizer.calculate(realSit, direction, startTime,
                        new LongSimpleManeuver.CheckFunction() {
                            @Override
                            public double run(double thrustAbs) {
                                return circulizeCheckFunction(direction, thrustAbs, startTime);
                            }
                        });
            case VIRTUAL:
                return circulizer.calculate(deepVirtualSit, direction, startTime,
                        new LongSimpleManeuver.CheckFunction() {
                            @Override
                            public double run(double thrustAbs) {
                                return circulizeCheckFunction(direction, thrustAbs, startTime);
                            }
                        });
            default:
                return null;
        }
    }

    private double ortogonalCheckFunction(Vector3 startVector, Vector3 addVector, Vector3 needOrt, double abs) {
        Vector3 addResult = startVector.add(addVector.multiply(abs));
        return Math.abs(needOrt.scalarMultiply(addResult));
    }

    public SingularNode changePlain(Vector3 newPlainNormal, ManeuverType type) {
        while (!(Math.abs(newPlainNormal.scalarMultiply(virtVessel.getRelativeCoord())) < 0.1)) {
            virtSit.nextMinute();
        }
        System.out.println("Scalar between got plain normal and new plain normal:"
                + Math.abs(newPlainNormal.scalarMultiply(virtVessel.getRelativeTrajectory().getPlainNormal())));
        Vector3 startVector = virtVessel.getRelativeSpeed().direction();
        Vector3 addVector = virtVessel.getRelativeTrajectory().getPlainNormal().direction();
        GoldenSection goldenOrtogonalizer = new GoldenSection();
        double abs = virtVessel.getRelativeSpeed().abs();
        double absDir = goldenOrtogonalizer.findPoint(
                -1.E3,
                1.E3,
                new GoldenSection.GoldenFunction() {
                    @Override
                    public double run(double point) {
                        return ortogonalCheckFunction(startVector, addVector, newPlainNormal, point);
                    }
                },
                null,
                null);
        Vector3 direction = startVector.add(addVector.multiply(absDir)).direction();
        VesselState wish = new VesselState(
                direction.multiply(abs),
                virtVessel.getRelativeCoord(),
                virtSit.whatTime());
        virtualModelReset();
        switch (type) {
            case REAL:
                return new SingularNode(wish, realSit);
            case VIRTUAL:
                return new SingularNode(wish, deepVirtualSit);
            default:
                return null;
        }
    }

    private void deepVirtualNodeExecute(SingularNode virtualNode) {
        while (!virtualNode.checkEnded()) {
            virtualNode.execute(deepVirtualSit.whatTime());
            deepVirtualSit.nextMinute();
        }
    }

    public NodeSeries seriesTest() {
        Queue<SingularNode> series = new LinkedList<>();
        series.offer(changePlain(new Vector3(1, 0, 1), ManeuverType.REAL));
        deepVirtualNodeExecute(changePlain(new Vector3(1, 0, 1), ManeuverType.VIRTUAL));
        virtSit = new ModelSit(deepVirtualSit);
        series.offer(changePlain(new Vector3(0, 0, 1), ManeuverType.REAL));
        return new NodeSeries(series,realSit);

    }

    public SingularNode changeTime(Vector3 thrust, long startTime)
    {
        double thrustAbs = thrust.abs();
        virtSit.waitTillTime(startTime);
        Vector3 newThrust = virtVessel.getRelativeSpeed().direction().multiply(thrustAbs);
        virtualModelReset();
        return new SingularNode(newThrust,startTime,realSit);
    }
}
