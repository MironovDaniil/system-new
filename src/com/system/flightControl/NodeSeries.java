package com.system.flightControl;

import com.system.model.ModelSit;
import com.system.model.Rocket;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.util.Queue;

/**
 * Created by Varamadon on 02.03.2018.
 */
public class NodeSeries {
    private Queue<SingularNode> series;
    private ModelSit model;
    private SingularNode current;
    private boolean hasEnded;
    private boolean fail;

    public NodeSeries(Queue<SingularNode> series, ModelSit model) {
        this(series, model, null, false, false);
        if (this.series.peek() == null) {
            hasEnded = true;
            System.out.println("End Before Start");
        }
        current = this.series.poll();
        if (current.checkFail()) {
            fail = true;
            System.out.println("FAIL Series");
        }
        //System.out.println("Node created");
    }

    public NodeSeries(Queue<SingularNode> series, ModelSit model, SingularNode current, boolean hasEnded, boolean fail) {
        this.series = series;
        this.model = model;
        this.current = current;
        this.hasEnded = hasEnded;
        this.fail = fail;
    }

    public boolean watchEnd() {
        return hasEnded;
    }

    public boolean checkFail() {
        return fail;
    }

    public void execute(long currentTime) {
        //System.out.println("Series execute started");
        if (hasEnded) {
            return;
        }
        if (!current.execute(currentTime)) {
            //System.out.println("Node ended");
            fail = current.checkFail();
            if (series.peek() == null) {
                hasEnded = true;
                //System.out.println("Series ended");
            }
            if (hasEnded) {
                return;
            }
            current = series.poll();
            //System.out.println("Next node");
            this.execute(currentTime);
        }
    }
}
