package com.system.flightControl;

import com.system.model.Constans;
import com.system.model.Vector3;

/**
 * Created by Varamadon on 16.04.2018.
 */
public class GoldenSection {
    public double findPoint(double lesser, double bigger, GoldenFunction function,
                            Double previousPoint, Double previousPointFun) {
        if (Math.abs(bigger - lesser) < 0.001 * Math.abs(bigger)) {
            System.out.println("Finder finished! Stopping difference:" + Math.abs(bigger - lesser));
            System.out.println("Lesser:" + lesser);
            System.out.println("Bigger:" + bigger);
            //System.out.println("Minimized function:" + function.run((bigger + lesser) / 2));
            return (bigger + lesser) / 2;
        }
        if (lesser > bigger) {
            double t = lesser;
            lesser = bigger;
            bigger = t;
        }
        //System.out.println("Finder working!");
        double lesserNew = bigger - ((bigger - lesser) / Constans.GOLDEN_SECTION_CONSTANT);
        double biggerNew = lesser + ((bigger - lesser) / Constans.GOLDEN_SECTION_CONSTANT);
        double funLesser, funBigger;
        if (previousPoint == null) {
            funLesser = function.run(lesserNew);
            funBigger = function.run(biggerNew);
        } else {
            if (Math.abs(lesserNew - previousPoint) < 0.0001 * previousPoint) {
                funLesser = previousPointFun;
                //System.out.println("Lesser optimized!");
            } else funLesser = function.run(lesserNew);
            if (Math.abs(biggerNew - previousPoint) < 0.0001 * previousPoint) {
                funBigger = previousPointFun;
                //System.out.println("Bigger optimized!");
            } else funBigger = function.run(biggerNew);
        }
        System.out.println("Lesser:"+lesserNew);
        System.out.println("Function in lesser:"+funLesser);
        System.out.println("Bigger:"+biggerNew);
        System.out.println("Function in bigger:"+funBigger);
        if (funBigger == 0) return biggerNew;
        if (funLesser == 0) return lesserNew;
        if (funLesser >= funBigger) {
            return findPoint(lesserNew, bigger, function, biggerNew, funBigger);
        } else return findPoint(lesser, biggerNew, function, lesserNew, funLesser);
    }

    public interface GoldenFunction {
        double run(double point);
    }
}
