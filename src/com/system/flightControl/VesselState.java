package com.system.flightControl;


import com.system.model.Vector3;

/**
 * Created by Varamadon on 22.02.2018.
 */
public class VesselState {
    private Vector3 speed;
    private Vector3 coord;
    private long time;

    public VesselState(Vector3 speed, Vector3 coord, long time) {
        this.speed = speed;
        this.coord = coord;
        this.time = time;
    }

    public Vector3 getSpeed() {
        return speed;
    }

    public Vector3 getCoord() {
        return coord;
    }

    public long getTime() {
        return time;
    }

    public void show() {
        speed.show();
        //coord.show();
        //System.out.println(time);
    }


}
