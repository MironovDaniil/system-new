package com.system.flightControl;

import com.system.model.Constans;
import com.system.model.ModelSit;
import com.system.model.Vector3;

/**
 * Created by Varamadon on 17.04.2018.
 */
public class LongSimpleManeuver {

    private GoldenSection goldenSectionMinimizer;

    public LongSimpleManeuver(GoldenSection goldenSectionMinimizer) {
        this.goldenSectionMinimizer = goldenSectionMinimizer;
    }

    public SingularNode calculate(final ModelSit model,
                                  Vector3 direction,
                                  long startTime,
                                  CheckFunction check) {
        if (model.whatTime() > startTime) {
            System.out.println("Complex maneuver failed - too late!");
            return null;
        }
        System.out.println("Complex maneuver calculation start");
        System.out.println("Direction vector:");
        direction.show();
        ModelSit virtual = new ModelSit(model);
//        System.out.println("Start time:"+startTime);
//        System.out.println("Current time:"+virtual.whatTime());
        virtual.waitTillTime(startTime);
        System.out.println("Model moved to start time");
        double startPoint = virtual.getRocket().getMaxThrust();
        double abs = goldenSectionMinimizer.findPoint(0, 1.E30 * startPoint,
                new GoldenSection.GoldenFunction() {
                    @Override
                    public double run(double point) {
                        return check.run(point);
                    }
                },null,null);
        System.out.println("Calculation finished");
        System.out.println("=============================");
        VesselState wish = new VesselState(direction.multiply(abs),
                virtual.getRocket().getRelativeCoord(),
                startTime);
        return new SingularNode(direction.multiply(abs),startTime,model);
    }

    public interface CheckFunction {
        double run(double thrustAbs);
    }
}


