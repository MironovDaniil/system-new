package com.system.flightControl;

import com.system.model.Constans;
import com.system.model.ModelSit;
import com.system.model.Rocket;
import com.system.model.Vector3;

/**
 * Created by Varamadon on 14.04.2018.
 */
public class SingularNode {
    private final VesselState wish;
    private final Vector3 preCalculatedThrust;
    private final boolean precalculated;
    private ModelSit model;
    private boolean ended;
    private boolean fail;


    public SingularNode(VesselState wish, ModelSit model) {
        this(wish, null, false, model, false, false);
        if (wish.getTime() < model.whatTime()) {
            fail = true;
        }
    }

    public SingularNode(Vector3 preCalculatedThrust, long startTime, ModelSit model) {
        this(new VesselState(Constans.ZERO, Constans.ZERO, startTime), preCalculatedThrust, true, model, false, false);
        if (wish.getTime() < model.whatTime()) {
            fail = true;
        }
    }

    public SingularNode(VesselState wish, Vector3 preCalculatedThrust, boolean precalculated, ModelSit model, boolean ended, boolean fail) {
        this.wish = wish;
        this.preCalculatedThrust = preCalculatedThrust;
        this.precalculated = precalculated;
        this.model = model;
        this.ended = ended;
        this.fail = fail;
    }

    private Vector3 calculate() {
        Vector3 difference = wish.getSpeed().add(model.getRocket().getRelativeSpeed().multiply(-1));
        return difference.add(model.getRocket().getAcceleration().multiply(-1));
    }

    private Vector3 advancedCalculate() {
        GoldenSection finder = new GoldenSection();
        System.out.println("finder created!");
        Vector3 direction = calculate().direction();
        return calculate().direction().multiply(
                finder.findPoint(-1.E+6 * calculate().abs(), 1.E+6 * calculate().abs(),
                        new GoldenSection.GoldenFunction() {
                            @Override
                            public double run(double point) {
                                return checkDist(point, direction);
                            }
                        }, null, null));
    }

    private boolean check(Rocket rocket) {
        double got = rocket.getRelativeSpeed().dist(wish.getSpeed());
        /*System.out.println("Real coordinate:");
        rocket.getRelativeCoord().show();
        System.out.println("Wished coordinate");
        wish.getCoord().show();
        System.out.println("Real speed:");
        rocket.getRelativeSpeed().show();
        System.out.println("Wished speed:");
        wish.getSpeed().show();
        System.out.println("Checking, got coordinate:" + rocket.getRelativeCoord().dist(wish.getCoord()));*/
        System.out.println("Checking, got speed:" + got);
        double need = 0.001 * wish.getSpeed().abs();
        System.out.println("Need under" + need);
        return (got < need);
    }

    private double checkDist(double abs, Vector3 direction) {
        ModelSit virtual = new ModelSit(model);
        virtual.getRocket().setThrust(direction, abs * virtual.getRocket().getMass());
        virtual.nextMinute();
        //System.out.println("Current thrust power:"+abs);
        //virtual.getRocket().getRelativeSpeed().show();
        return virtual.getRocket().getRelativeSpeed().dist(wish.getSpeed());
    }

    public boolean checkFail() {
        return fail;
    }

    public boolean checkEnded() {
        return ended;
    }

    public boolean execute(long time) {
        if (wish.getTime() - 1 > time) {
            return true;
        }
        if (wish.getTime() - 1 == time) {
            Vector3 thrust;
            if (precalculated) {
                thrust = preCalculatedThrust;
            } else {
                thrust = advancedCalculate();
            }
            //System.out.println("Calculate result:" + calculate().abs());
            System.out.println("Wished speed:");
            wish.getSpeed().show();
            System.out.println("Real speed:");
            model.getRocket().getRelativeSpeed().show();
            System.out.println("Singular delta v:" + thrust.abs());
            System.out.println("Singular thrust:");
            thrust.show();
            /*System.out.println("==============================");
            check(model.getRocket());
            System.out.println("==============================");*/
            model.getRocket().setThrust(thrust.direction(), thrust.abs() * model.getRocket().getMass());
            System.out.println("Singular node executed!");
            return true;
        }
        if (wish.getTime() == time) {
            model.getRocket().setThrust(Constans.ZERO, 0);
            ended = true;
            if (precalculated) {
                System.out.println("Singular node succeeded:" + true);
            } else {
                System.out.println("Singular node succeeded:" + check(model.getRocket()));
            }

        }
        if ((wish.getTime() - 1 < time) && (!ended)) {
            fail = true;
            ended = true;
            //System.out.println("Fail singular node!");
        }
        return false;
    }


}
