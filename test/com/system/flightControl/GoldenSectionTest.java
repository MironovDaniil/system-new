package com.system.flightControl;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Varamadon on 19.04.2018.
 */
public class GoldenSectionTest {
    private GoldenSection goldenSection;

    @Before
    public void setUp() {
        goldenSection = new GoldenSection();
    }

    @Test
    public void findPoint_linearFunc_findsMinimum() {
        double result = goldenSection.findPoint(-100, 100, new GoldenSection.GoldenFunction() {
            @Override
            public double run(double point) {
                return 5 * point;
            }
        }, null, null);
        assertEquals(-100,result,0.1);
    }

}