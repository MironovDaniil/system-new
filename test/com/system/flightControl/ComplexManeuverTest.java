package com.system.flightControl;

import org.junit.Before;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

/**
 * Created by Varamadon on 20.04.2018.
 */
public class ComplexManeuverTest {

    private LongSimpleManeuver complexManeuver;
    private GoldenSection goldenSection;

    @Before
    public void setUp() throws Exception {
        goldenSection = Mockito.mock(GoldenSection.class);
        complexManeuver = new LongSimpleManeuver(goldenSection);
        Mockito.when(goldenSection.findPoint(
                eq(3),
                eq(5),
                any(GoldenSection.GoldenFunction.class),
                null,
                null))
                .thenReturn(5.);
    }

}